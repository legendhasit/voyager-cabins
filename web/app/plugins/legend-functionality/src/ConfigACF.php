<?php

namespace Legend\Functionality;

class ConfigACF {

    function __construct() {


        /**
        * Set local json save path
        * @param  string $path unmodified local path for acf-json
        * @return string       our modified local path for acf-json
        */
        add_filter('acf/settings/save_json', function ($path) {
          return LEGEND_FUNCTIONALITY_PLUGIN_DIR . 'src/field-groups';
        });

        /**
        * Set local json load path
        * @param  string $path unmodified local path for acf-json
        * @return string       our modified local path for acf-json
        */
        add_filter('acf/settings/load_json', function ($paths) {
          unset($paths[0]);
          $paths[] = LEGEND_FUNCTIONALITY_PLUGIN_DIR . 'src/field-groups';
          return $paths;
        });

    }


}