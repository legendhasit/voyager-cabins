<?php

namespace Legend\Functionality;

class PostTypeBuilder {

    function __construct() {

        // Include all post types
        foreach (glob(LEGEND_FUNCTIONALITY_PLUGIN_DIR . "src/types/*.php") as $filename) {
          include $filename;
        }

    }
}