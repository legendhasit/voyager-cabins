<?php

use PostTypes\PostType;

// Post type options
$options = [
	'supports' => [ 'title', 'editor', 'page-attributes', 'thumbnail', 'custom-fields' ],
	// 'capability_type' => 'page',
	'public' => true

];

// Register post type
$instagram = new PostType('instagram', $options);

// Set post type dashicon from Dashicons: https://developer.wordpress.org/resource/dashicons/#chart-bar
$instagram->icon('dashicons-format-image');

// Register the "instagram" post type with WordPress
$instagram->register(); 
