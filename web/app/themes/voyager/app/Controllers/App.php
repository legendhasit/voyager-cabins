<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        if (is_single()) {
            return __('Blog', 'sage');
        }
        return get_the_title();
    }

    public function socialMedia() {
        return $social_media = [
            'facebook' => get_field('sm_facebook', 27),
            'instagram' => get_field('sm_instagram', 27),
            'youtube' => get_field('sm_youtube', 27)
        ];
    }

    public function contactInfo() {
        return $contact_info = [
            'phone'          => get_field('ci_phone', 27),
            'email'          => get_field('ci_email', 27),
            'street_address' => get_field('ci_street_address', 27),
            'postal_address' => get_field('ci_postal_address', 27),
            'opening_hours'  => get_field('ci_opening_hours', 27)
        ];
    }

    public function pageHeaderBackground() {

        if(is_home()) {
            $id = get_option('page_for_posts');
        }
        else {
            $id = get_the_id();
        }

        return $page_header_background = [
            'image' => get_field('ph_background', $id),
            'tint' => get_field('ph_tint', $id),
        ];
    }

    public function featureBanner() {
        return $feature_banner = [
            'hide_content' => get_field('fb_hide_content'),
            'light_text' => get_field('fb_light_text'),
            'bold_text'  => get_field('fb_bold_text'),
            'fancy_text' => get_field('fb_fancy_text'),
            'button'     => get_field('fb_button'),
            'gallery'    => get_field('fb_gallery'),
            'background_tint'    => get_field('fb_background_tint')
        ];
    }

    public static function getLatestInstagrams() {

        $args = [
            'post_type' => 'instagram',
            'posts_per_page' => 4
        ];

        $the_query = new \WP_Query( $args );

        $latest_instagrams = [];

        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {

                $the_query->the_post();

                // get WP ID of instagram post
                $id = get_the_ID();

                $instagram_link = get_post_meta(get_the_ID(), 'instagram_link', true);
                $title = get_the_title();

                // get WP image src of instagram post
                $thumb_id = get_post_thumbnail_id();
                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium_square', true);
                $image = $thumb_url_array[0];

                $instagram_post = [
                    'title' => $title,
                    'link' => $instagram_link,
                    'image' => $image
                ];

                array_push($latest_instagrams, $instagram_post);

            }
            /* Restore original Post Data */
            wp_reset_postdata();

            return $latest_instagrams;

        } else {
            // no posts found
            return '';
        }

    }
}
