<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{

    public function whyUs() {
        return $why_us = [
            'image'      => get_field('wu_image'),
            'heading'    => get_field('wu_heading'),
            'fancy_text' => get_field('wu_fancy_text'),
            'content'    => get_field('wu_content')
        ];
    }

    public function ourStory() {
        return $our_story = [
            'image'      => get_field('os_image'),
            'heading'    => get_field('os_heading'),
            'fancy_text' => get_field('os_fancy_text'),
            'content'    => get_field('os_content')
        ];
    }

    public function fullWidthSlider() {
        return get_field('fwis_images');
    }

    public function fullWidthContent() {
        return $full_width_content = [
            'heading' => get_field('fwc_heading'),
            'content' => get_field('fwc_content'),
        ];
    }
}
