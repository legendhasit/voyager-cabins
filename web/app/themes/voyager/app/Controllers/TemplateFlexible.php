<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateFlexible extends Controller
{

    public function flexibleContent() {
        return get_field('content_blocks');
    }

}
