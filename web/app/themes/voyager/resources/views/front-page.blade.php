@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
    @include('partials.feature-banner')

    <section id="welcome">
      <img src="@asset('images/wing-right.svg')" height="400" alt="Voyager wing right">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-lg-auto text-center">
            <h2>{{ $full_width_content['heading'] }}</h2>
            {!! $full_width_content['content'] !!}
            {{-- <img class="mt-5" src="@asset('images/watermark.svg')" height="30" alt="Voyager Watermark"> --}}
          </div>
        </div>
      </div>
      <img src="@asset('images/wing-left.svg')" height="400" alt="Voyager wing left">
    </section>

    <div id="whyUs" class="slanted-section slanted-section--reversed">

      <div class="slanted-section__image" style="background-image: url({{$why_us['image']['url'] }});"></div>

      <div class="slanted-section__content">
        <h2>{{ $why_us['heading'] }} <span class="fancy-text pl-2">{{ $why_us['fancy_text'] }}</span></h2>
        {!! $why_us['content'] !!}
      </div>
    </div>

    <section class="full-width-slider py-0 mb-0">
      @foreach ($full_width_slider as $item)
        <div class="slide" style="background-image: url({{ $item['url'] }});"></div>
      @endforeach
    </section>

    <div id="ourStory" class="slanted-section">
      <div class="slanted-section__image" style="background-image: url({{$our_story['image']['url'] }});"></div>
      <div class="slanted-section__content">
        <h2>{{ $our_story['heading'] }} <span class="fancy-text pl-2">{{ $our_story['fancy_text'] }}</span></h2>
        {!! $our_story['content'] !!}
      </div>
      <img src="@asset('images/small-wing-left.svg')" height="400" alt="Left wing">
    </div>

    <section id="journey" class="bg-light">
      <img src="@asset('images/cabin.svg')" height="66" alt="Cabin icon">
      <div class="container">
        <div class="d-flex justify-content-center">
          <h2>The <span class="fancy-font pl-2">{{get_the_title(25)}}</span></h2>
          <div class="pl-4">
            <a href="{{ get_the_permalink(25) }}" class="btn btn-round btn-outline-primary">See all</a>
          </div>
        </div>
        <div class="journey__slider">

          @php $the_query = new WP_Query(['posts_per_page' => -1]); @endphp

          @if ($the_query->have_posts())
            @while ($the_query->have_posts())
              @php $the_query->the_post(); @endphp
              @include('partials.content')
            @endwhile
            @php wp_reset_postdata(); @endphp
          @endif

        </div>

      </div>
    </section>


  @endwhile
@endsection
