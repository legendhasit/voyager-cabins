{{--
  Template Name: Flexible Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.feature-banner')


    @if ($flexible_content)
      @foreach ($flexible_content as $block)
        @include('partials.blocks.' . $block['acf_fc_layout'])
      @endforeach
    @endif


  @endwhile
@endsection
