<div id="flexibleBlock{{ $loop->iteration }}" class="two-col slanted-section">
  <div class="slanted-section__image" style="background-image: url({{$block['image']['url'] }});"></div>

  <div class="slanted-section__content">

    @if ($block['show_house_icon'])
      <img src="@asset('images/cabin.svg')" height="66" alt="Cabin icon">
    @endif

    {!! $block['content'] !!}

    @if ($block['button'])
      <a href="{{ $block['button']['url'] }}" {{ ($block['button']['target']) ? ' target=_blank rel=noopener' : '' }}  class="mt-3 btn btn-primary btn-round">{{ $block['button']['title'] }}</a>
    @endif
  </div>
</div>
