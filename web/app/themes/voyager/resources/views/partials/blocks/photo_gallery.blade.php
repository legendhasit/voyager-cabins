<section id="flexibleBlock{{ $loop->iteration }}" class="gallery-block">
  <div class="container">

    <div class="row">
      @foreach ($block['images'] as $item)
        <div class="{{ count($block['images']) > 1 ? 'col-md-6 col-lg-4 mb-3 mb-sm-4' : 'col-12 mb-3' }}">
          <a class="zoom-hover embed-responsive embed-responsive-4by3" href="{{ $item['url'] }}" data-toggle="lightbox" data-gallery="post-gallery" data-footer="{{ $item['alt'] }}">
            {!! wp_get_attachment_image($item['id'], 'full', false, ['class' => 'embed-responsive-item object-cover']) !!}
          </a>
        </div>
      @endforeach
    </div>
  </div>
</section>
