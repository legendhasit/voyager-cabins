<section id="flexibleBlock{{ $loop->iteration }}" class="cta text-center bg-light">
  <div class="container">
    <h3>{{ $block['text'] }}</h3>
    @if ($block['button'])
      <a href="{{ $block['button']['url'] }}" {{ ($block['button']['target']) ? ' target=_blank rel=noopener' : '' }}  class="mt-3 btn btn-primary btn-round">{{ $block['button']['title'] }}</a>
    @endif
  </div>
</section>
