<section id="flexibleBlock{{ $loop->iteration }}" class="icon-grid text-center">
  <div class="container">
    <h2 class="mb-3">{!! $block['heading'] !!}</h2>

    @if ($block['item'])
      <div class="row">
        @foreach ($block['item'] as $item)
            <div class="icon-grid__col col-sm-6 col-lg-3 mt-5">
              <div class="icon-grid__item">
                <img class="icon mb-4" src="{{ $item['icon']['sizes']['medium'] }}" alt="{{ $item['icon']['alt'] }}">
                <h5 class="mb-3">{!! $item['title'] !!}</h5>
                <p class="mb-0">{!! $item['content'] !!}</p>
              </div>
            </div>
        @endforeach
      </div>
    @endif

  </div>
</section>
