<section id="flexibleBlock{{ $loop->iteration }}" class="basic-content">
  <div class="container text-center">
    <div class="row">
      <div class="col-lg-8 mx-auto">
        @if ($block['icon'])
          <img class="mb-4" height="32" src="{{ $block['icon']['sizes']['medium'] }}" alt="$block['icon']['alt']">
        @endif
        {!! $block['content'] !!}

        @if ($block['button'])
          <a href="{{ $block['button']['url'] }}" {{ ($block['button']['target']) ? ' target=_blank rel=noopener' : '' }}  class="mt-3 btn btn-primary btn-round">{{ $block['button']['title'] }}</a>
        @endif

      </div>
    </div>
  </div>
</section>
