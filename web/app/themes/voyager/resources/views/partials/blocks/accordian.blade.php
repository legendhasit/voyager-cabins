@php
  $blockID = $loop->iteration;
@endphp

<section id="flexibleBlock{{ $loop->iteration }}" class="flexible-section">
  <div class="container">
    <div class="text-center">
      <h2>{!! $block['heading'] !!}</h2>
      <img class="accordion__icon" src="{{ $block['icon']['sizes']['medium'] }} " alt="{{ $block['icon']['alt'] }}">
    </div>

    @if ($block['expandable_items'])
      <div class="accordion" id="accordion{{ $blockID }}">

        <div class="row d-flex justify-content-around">

          @foreach ($block['expandable_items'] as $item)


            @if ($loop->iteration == 1 || ($loop->iteration) % 4 == 0)
              <div class="col-md-6 col-lg-5">
            @endif

              <div class="card">
                <div class="card-header">
                  <h4 class="mb-0 accordion__heading">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$loop->iteration}}" aria-expanded="false" aria-controls="collapse{{$loop->iteration}}">
                      {!! $item['title'] !!}
                    </button>
                  </h4>
                </div>
                <div id="collapse{{$loop->iteration}}" class="collapse" aria-labelledby="heading{{$loop->iteration}}" data-parent="#accordion{{ $blockID }}">
                  <div class="card-body">
                      {!! $item['content'] !!}
                  </div>
                </div>
              </div>


            @if (($loop->iteration) % 3 == 0)
              </div>
            @endif


          @endforeach

        </div>

      </div>
    @endif

  </div>
</section>
