@php 
  $post_classes = get_post_class(); 
  $flexible_content = get_field('a_flexible_content');
@endphp

<article class="{{ implode(' ', $post_classes) }}">
  <div class="row">
    <div class="col-lg-10 mx-auto">
      <header class="text-center mb-4">
        <h2 class="mb-2">{!! get_the_title() !!}</h2>
        <h6>{{ get_the_date() }}</h6>
      </header>
      <div class="entry-content">
        @if ($flexible_content)
          @foreach ($flexible_content as $item)

              @switch($item['acf_fc_layout'])
                  @case('basic_content')
                      {!! $item['fc_content'] !!}
                      @break
                  @case('gallery')
                      @php
                        $gallery = $item['fc_gallery'];
                      @endphp
                      <div class="row">
                        @include('partials.flexible-layouts.gallery')
                      </div>
                      @break
                  @default  
              @endswitch

          @endforeach
        @endif
      </div>
    </div>
  </div>
</article>
