@include('partials.latest-instagrams')


<footer class="site-footer text-center text-lg-left">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 pt-2">
        <p class="text-primary">Copyright &copy; {{ date('Y') }} Voyager Cabins | Website by <a href="https://www.legendhasit.co.nz">Legend</a>.</p>
      </div>
      <div class="col-lg-6 text-lg-right">
        <div class="mb-3">
          <p class="text-primary d-lg-inline-block mb-lg-0">{{ $contact_info['street_address'] }}</p>
          <img class="mb-3" src="@asset('images/watermark.svg')" alt="Voyager Watermark">
        </div>
      </div>
    </div>
  </div>
</footer>
