@php $latest_instagrams = App::getLatestInstagrams(); @endphp

<section id="latestInstagrams">
    <img src="@asset('images/small-wing-right.svg')" alt="Triangle">
    <div class="container">
        <h2 class="text-center">Follow us on social media #voyagercabins</h2>

        @include('partials/social-icons')

        <div class="row pt-4">
            @foreach ($latest_instagrams as $item)
                <div class="col-sm-6 col-lg-3 mt-gutter">
                    <a target="_blank" rel="noopener" aria-label="Link to instagram post" class="zoom-hover" href="{{ $item['link'] }}">
                        <img class="img-fluid" src="{{ $item['image'] }}" alt="{!! $item['title'] !!}">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>