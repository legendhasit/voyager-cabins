@php 
  if (! $page_header_background['image']['url']) {
    $page_header_background['image']['url'] = \App\asset_path('images/page-header-bg.jpg'); 
  }

  $tint_class = ($page_header_background['tint']) ? 'page-header--tint' : '';
@endphp


<div class="page-header {{ $tint_class }}" style="background-image: url({{ $page_header_background['image']['url'] }});">
  <div class="container">
    <h1 class="fancy-font">{!! App::title() !!}</h1>
  </div>
</div>
