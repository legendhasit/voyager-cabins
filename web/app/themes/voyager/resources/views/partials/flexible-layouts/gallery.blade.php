@foreach ($gallery as $item)
  @if (count($gallery) > 1)
    <div class="col-md-6 mb-4">
    @php
      $size = 'medium_large_crop';
    @endphp
    @else
    <div class="col-12 mb-3">
      @php
        $size = 'large';
      @endphp
  @endif
    <a class="zoom-hover" href="{{ $item['url'] }}" data-toggle="lightbox" data-gallery="post-gallery" data-footer="{{ $item['alt'] }}">
      <img class="img-fluid" src="{{ $item['sizes'][$size] }}" alt="{{ $item['alt'] }}">
    </a>
  </div>
@endforeach