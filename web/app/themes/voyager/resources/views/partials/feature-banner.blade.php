@php $tint_class = ($feature_banner['background_tint']) ? 'feature-banner--tinted' : '';  @endphp
<section class="feature-banner {{ $tint_class }}">
  <div class="feature-banner__slider">
    @foreach ($feature_banner['gallery'] as $item)
      <div class="feature-banner__slide" style="background-image: url({{$item['url']}});"></div>
    @endforeach
  </div>
  @if (!$feature_banner['hide_content'])
    <div class="feature-banner__content">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <h2>
              <span class="font-weight-light">{{$feature_banner['light_text']}}</span>
              <span class="font-weight-semi">{{$feature_banner['bold_text']}}</span>
              <span class="fancy-font">{{$feature_banner['fancy_text']}}</span>
            </h2>
            <a href="{{$feature_banner['button']['url']}}" class="p-3 btn btn-primary btn-round">{{$feature_banner['button']['title']}}</a>
          </div>
        </div>
      </div>
    </div>
  @endif
</section>
