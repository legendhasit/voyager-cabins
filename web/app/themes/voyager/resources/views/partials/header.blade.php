<header class="site-header">
  <div class="container position-relative">
    <div class="row mb-4">
      <div class="ml-lg-auto col-7 col-sm-5 col-md-4 col-lg-4 d-flex align-items-center justify-content-lg-center">
        <a class="brand d-block" aria-label="Voyager Cabins logo" href="{{ home_url('/') }}"><img class="img-fluid" src="@asset('images/logo.png')" alt="Voyager Cabins logo"></a>
      </div>
      <div class="d-flex col-5 col-sm-4 col-lg-4 order-sm-last flex-column align-items-end justify-content-between">
        @include('partials.social-icons')
        <p class="d-none d-sm-block text-right pt-3">Be the first to hear<br>about our latest news</p>
        <!-- Button trigger modal -->
        <button type="button" class="px-3 px-sm-4 btn btn-outline-primary btn-round" data-toggle="modal" data-target="#subscribeModal">Sign up</button>
      </div>
    </div>
    <button aria-label="Toggle Navigation Menu" class="d-block d-lg-none hamburger hamburger--slider" type="button">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>
  </div>
  <div id="mainMenu" class="bg-primary">
    <div class="container-fluid">
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav justify-content-center']) !!}
        @endif
      </nav>
    </div>
  </div>
</header>
