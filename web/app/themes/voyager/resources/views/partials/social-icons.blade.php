<div class="social-icons">
    <a target="_blank" rel="noopener" aria-label="{{ $social_media['facebook']['title'] }}" href="{{ $social_media['facebook']['url'] }}">
        {!! file_get_contents(\App\asset_path('images/facebook.svg')) !!}
    </a>
    <a target="_blank" rel="noopener" aria-label="{{ $social_media['instagram']['title'] }}" href="{{ $social_media['instagram']['url'] }}">
        {!! file_get_contents(\App\asset_path('images/instagram.svg')) !!}
    </a>
    {{-- <a target="_blank" rel="noopener" aria-label="{{ $social_media['youtube']['title'] }}" href="{{ $social_media['youtube']['url'] }}">
        {!! file_get_contents(\App\asset_path('images/youtube.svg')) !!}
    </a> --}}
</div>