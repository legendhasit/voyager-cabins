@php 
  $post_classes = get_post_class();
  array_push($post_classes, 'card mb-4');
  $gallery = get_field('a_gallery', get_the_id());
@endphp

<article class="{{ implode(' ', $post_classes) }}">
  <a class="zoom-hover" href="{{ get_permalink() }}">
    {!! get_the_post_thumbnail(get_the_id(), 'medium_large_crop', ['class' => 'card-img-top img-fluid']) !!}
    {{-- <img class="card-img-top" src="{{$gallery[0]['sizes']['medium_large_crop']}}" alt="{{$gallery[0]['alt']}}"> --}}
  </a>
  <div class="card-body">
    <div data-mh="my-group">
      <h5 class="card-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h5>
      <p  class="card-text pb-3">{!! get_post_meta(get_the_id(), '_genesis_description', true) !!}</p>
    </div>
    <a href="{{ get_permalink() }}" class="btn btn-primary btn-round">Read more</a>
  </div>
</article>
      