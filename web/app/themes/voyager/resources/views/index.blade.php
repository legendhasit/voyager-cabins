@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <section class="pb-0">
    <div class="container">
      @if (!have_posts())
        <div class="alert alert-warning">
          {{ __('Sorry, no results were found.', 'sage') }}
        </div>
        {!! get_search_form(false) !!}
      @endif
        
      <div class="row">
        @while (have_posts()) @php the_post() @endphp
          <div class="col-md-6 col-lg-4">
            @include('partials.content-'.get_post_type())
          </div>
        @endwhile
      </div>
    
      {!! get_the_posts_navigation() !!}
    </div>
  </section>


@endsection
