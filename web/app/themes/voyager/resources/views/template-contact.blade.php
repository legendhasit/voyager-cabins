{{--
  Template Name: Contact Page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')

    <section id="contactUs" class="bg-light">
        <img src="@asset('images/wing-right.svg')" height="500" alt="Voyager wing right">
        <div class="container">

          <div class="text-center row">
            <div class="col-lg-8 mx-lg-auto">
              {!! the_content() !!}
            </div>
          </div>

          <div class="row pt-4">
              <div class="col-lg-8 mx-lg-auto">
                  {!! do_shortcode('[contact-form-7 id="839" title="Contact form 1"]') !!}
              </div>
          </div>

        </div>

        <img src="@asset('images/wing-left.svg')" height="500" alt="Voyager wing left">
    </section>

  @endwhile
@endsection
