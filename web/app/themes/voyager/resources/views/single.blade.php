@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  @include('partials.page-header')

  <section id="singlePost" class="bg-light">
    <img src="@asset('images/wing-right.svg')" height="500" alt="Voyager wing right">
    <div class="container">
      @include('partials.content-single-'.get_post_type())
    </div>
    <img src="@asset('images/wing-left.svg')" height="500" alt="Voyager wing left">
  </section>

  @endwhile
@endsection
