export default {
  init() {
    // JavaScript to be fired on all pages

    //navigation menu
    $('.hamburger').click(function() {
      $(this).toggleClass('is-active');
      // $('#mainMenu').toggleClass('is-active');
      $('#mainMenu').slideToggle();
    });

    // init lightbox
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox();
    });

    $("a[href*='#']").mPageScroll2id();

    $('.full-width-slider').slick({
      arrows: false,
      dots: true,
    });

    $('.journey__slider').slick({

      slidesToShow: 3,
      arrow: true,
      infinite: false,

      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    //show modal on subscribe form submission.
    if ($('.mc4wp-response').html() != '') {
      $('#subscribeModal').modal('show');
    }

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
