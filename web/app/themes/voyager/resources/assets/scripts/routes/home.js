export default {
  init() {
    // JavaScript to be fired on the home page

    $('.feature-banner__slider').slick({
      autoplay: true,
      autoplaySpeed: 4000,
      infinite: true,
      fade: true,
      cssEase: 'linear',
      dots: false,
      arrow: false,
    });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
